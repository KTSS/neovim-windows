" Setting leader key
let mapleader = ","

call plug#begin('~/AppData/Local/nvim/plugged')
    Plug 'ap/vim-css-color'
    Plug 'morhetz/gruvbox'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
    Plug 'sheerun/vim-polyglot'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'vim-airline/vim-airline'
    Plug 'vimwiki/vimwiki'

    if has("nvim")
        Plug 'neovim/nvim-lspconfig'
        Plug 'nvim-lua/completion-nvim'
    endif

call plug#end()

" Basic setings
    set nocompatible
    packloadall
    filetype plugin indent on
    nnoremap c "_c
    set clipboard+=unnamedplus
    set colorcolumn=80
    set encoding=utf-8
    set go=P
    set gfn=PT\ Mono:h14
    set laststatus=0
    set mouse=a
    set noerrorbells
    set nohlsearch
    set noruler
    set noshowcmd
    set noshowmode
    set nowrap
    set number
    set relativenumber
    set title
    syntax on
    let g:gruvbox_contrast_hard = 'hard'
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    endif
    let g:gruvbox_invert_selection='0'

    colorscheme gruvbox
    set background=dark

" Cursor line:
    set cursorline
    set cursorcolumn

" Enable autocompletion
    set wildmode=longest,list,full

" Fixes splitting.
    set splitbelow splitright

" Improving vim just a little bit
    " 1 Yank, behave like normal, your ***ch
    nnoremap Y y$

    " 2 Keeping cursor centered
    nnoremap n nzzzv
    nnoremap N Nzzzv
    nnoremap J mzJ`z

    " 3 Undo break points
    inoremap , ,<c-g>u
    inoremap . .<c-g>u
    inoremap ! !<c-g>u
    inoremap ? ?<c-g>u
    inoremap ( (<c-g>u
    inoremap ) )<c-g>u
    inoremap [ [<c-g>u
    inoremap ] ]<c-g>u
    inoremap { {<c-g>u
    inoremap } }<c-g>u

    " 4 Jumplist mutations
    nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'
    nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'

    " 5 - Moving text without cluttering muh registars
    vnoremap  J          :m       '>+1<CR>gv=gv
    vnoremap  K          :m       '<-2<CR>gv=gv
    inoremap  <C-j>      <esc>:m  .+1<CR>==
    inoremap  <C-k>      <esc>:m  .-2<CR>==
    nnoremap  <leader>j  :m       .+1<CR>==
    nnoremap  <leader>k  :m       .-2<CR>==

" Open corresponding .pdf/.html or preview
    map <leader>p :!opout <c-r>%<CR><CR>

" Perform dot commands over visual blocks:
	vnoremap . :normal .<CR>
" By making neovim keep the undo tree, it is possible to open a document after
" you closed it and still  undo  your changes. If the program was compiled with
" support for it
  "if has("persistend_undo")
  "  set undodir="${XDG_CACHE_HOME:-$HOME/.cache}/neovim/undodir"
  "  set undofile
  "endif

" Replace all is aliased to S.
  nnoremap S :%s//g<Left><Left>

" Save file as sudo on files that require root permission
  cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Shortcutting split navigation, saving a keypress:
  map <C-h> <C-w>h
  map <C-j> <C-w>j
  map <C-k> <C-w>k
  map <C-l> <C-w>l

" Spell-check set to <leader>o, 'o' for 'orthography':
  map <leader>o :setlocal spell! spelllang=en_us<CR>

" Tab settings
  set expandtab
  set shiftwidth=4
  set softtabstop=4
  set tabstop=4

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
  if &diff
      highlight! link DiffText MatchParen
  endif

" Navigating with anchors
inoremap <leader><leader> <Esc>/<++><Enter>"_c4l
vnoremap <leader><leader> <Esc>/<++><Enter>"_c4l
map      <leader><leader> <Esc>/<++><Enter>"_c4l

" general insert commands
inoremap <leader>anc <++>

cnoreabbrev  Q      q
cnoreabbrev  Q!     q!
cnoreabbrev  Qall   qall
cnoreabbrev  Qall!  qall!
cnoreabbrev  W      w
cnoreabbrev  W!     w!
cnoreabbrev  WQ     wq
cnoreabbrev  Wa     wa
cnoreabbrev  Wq     wq
cnoreabbrev  wQ     wq

" Goyo
  " Goyo plugin makes text more readable when writing prose:
    map <leader>f :Goyo \| set bg=light \| set linebreak<CR>

  " Enable Goyo by default for mutt writting
    autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
    autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo | set bg=light
    autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
    autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

  " Enabling limelight when entering, disabling when leaving
  " Additional plugins can be configured in the commented elipsis line
  function! s:goyo_enter()
    if executable('tmux') && strlen($TMUX)
      silent !tmux set status off
      silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
    endif
    set noshowmode
    set noshowcmd
    set scrolloff=999
    Limelight
    colorscheme koehler
    " ...
  endfunction

  function! s:goyo_leave()
    if executable('tmux') && strlen($TMUX)
      silent !tmux set status on
      silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
    endif
    set showmode
    set showcmd
    set scrolloff=5
    Limelight!
    colorscheme koehler
    " ...
  endfunction

  autocmd! User GoyoEnter nested call <SID>goyo_enter()
  autocmd! User GoyoLeave nested call <SID>goyo_leave()

" Limelight
  " Highlighting of paragraphs
    let g:limelight_paragraph_span=1
  " Color name (:help cterm-colors) or ANSI code
    let g:limelight_conceal_ctermfg = 'gray'
    let g:limelight_conceal_ctermfg = 240
  " Color name (:help gui-colors) or RGB color
    let g:limelight_conceal_guifg = 'DarkGray'
    let g:limelight_conceal_guifg = '#666666'
  " Default: 0.5
    let g:limelight_default_coefficient = 0.7
  " Number of preceding/following paragraphs to include (default: 0)
    let g:limelight_paragraph_span = 1
  " Beginning/end of paragraph, when there's no empty line between the paragraphs, and each paragraph starts with indentation
    let g:limelight_bop = '^\s'
    let g:limelight_eop = '\ze\n^\s'
  " Highlighting priority (default: 10), set it to -1 not to overrule hlsearch
    let g:limelight_priority = -1
  " Integrate with Goyo
    autocmd! User GoyoEnter Limelight
    autocmd! User GoyoLeave Limelight!
  " Starts Limelight
  map <leader>l :Limelight

" vim-airline
  set laststatus=2

  let g:airline_extensions#tabline#enabled=1
  let g:airline_powerline_fonts=1

  " Starts Vim Fugitive
    nmap  <leader>gs   :G<CR>
    nmap  <leader>gj   :diffget //2<CR>
    nmap  <leader>gf   :diffget //3<CR>
    nmap  <leader>gc   :Gcheckout<CR>
    nmap  <leader>gp   :Git push<CR>
    nmap  <leader>gd   :Git diff :0<CR>

let g:vimwiki_url_maxsave = 0
map <leader>v :VimwikiIndex<CR>

" Disables automatic commenting on newline:
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Vertically center document when entering insert mode
  autocmd InsertEnter * norm zz

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
  autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
" vimwiki is controlled in its own vim file
  autocmd BufRead,BufNewFile *.md,*.markdown,*.rmd,/tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
  autocmd BufRead,BufNewFile *.ps1 set filetype=powershell
  autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
  autocmd BufRead,BufNewFile *.tex  set filetype=tex
  autocmd BufRead,BufNewFile *.wiki set filetype=wiki

" Automatically deletes all trailing whitespace and newlines at end of file on save.
  autocmd BufWritePre * %s/\n\+\%$//e
  autocmd BufWritePre * %s/\s\+$//e
  autocmd BufWritePre *.[ch] %s/\%$/\r/e
  autocmd BufWritepre * %s/\n\+\%$//e

  " Besides, skim.vim add the interactive version of ag and rg function, you
  " could add these lines to your vimrc and try out. Since I only have ripgrep:
  command! -bang -nargs=* Rg call fzf#vim#rg_interactive(<q-args>, fzf#vim#with_preview('right:50%:hidden', 'alt-h'))
  " Empty value to disable preview window altogether
  let g:fzf_preview_window  = ''
  " Always enable preview window on the right with 40% width
  let g:fzf_preview_window  = 'right:40%'
  " [Buffers] Jump to the existing window if possible
  let g:fzf_buffers_jump    = 1
  " [[B]Commits] Customize the options used by 'git log':
  let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
  " [Tags] Command to generate tags file
  let g:fzf_tags_command    = 'ctags -R'
  " [Commands] --expect expression for directly executing the command
  let g:fzf_commands_expect = 'alt-enter,ctrl-x'
  " files
  command! -bang -nargs=? -complete=dir Files call fzf#vim#files(<q-args>, <bang>0)
  " Source files
  command! -bang ProjectFiles call fzf#vim#files('~/.local/src', <bang>0)
  " Preview window with, e.g. bat
  command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, {'options': ['--layout=reverse', '--info=inline', '--preview', '~/.vim/plugged/fzf.vim/bin/preview.sh {}']}, <bang>0)
  " ripgrep with preview window
  command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview(), <bang>0)
  " Advanced ripgrep integration
  function! RipgrepFzf(query, fullscreen)
      let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
      let initial_command = printf(command_fmt, shellescape(a:query))
      let reload_command  = printf(command_fmt, '{q}')
      let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
      call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
  endfunction

  command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)
  " Mapping selecting mappings
  nmap <leader><tab> <plug>(fzf-maps-n)
  xmap <leader><tab> <plug>(fzf-maps-x)
  omap <leader><tab> <plug>(fzf-maps-o)
  " Insert mode completion
  imap <c-x><c-k> <plug>(fzf-complete-word)
  imap <c-x><c-f> <plug>(fzf-complete-path)
  imap <c-x><c-l> <plug>(fzf-complete-line)
  " Path completion with custom source command
  inoremap <expr> <c-x><c-f> fzf#vim#complete#path('fd')
  inoremap <expr> <c-x><c-f> fzf#vim#complete#path('rg --files')
  " Word completion with custom spec with popup layout option
  inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'window': { 'width': 0.2, 'height': 0.9, 'xoffset': 1 }})
  " Replace the default dictionary completion with fzf-based fuzzy completion
  inoremap <expr> <c-x><c-k> fzf#vim#complete('cat /usr/share/dict/words')
  " Custom statusline
  function! s:fzf_statusline()
      " Override statusline as you like
      highlight fzf1 ctermfg=161 ctermbg=251
      highlight fzf2 ctermfg=23  ctermbg=251
      highlight fzf3 ctermfg=237 ctermbg=251
      setlocal statusline=%#fzf1#\ >\ %#fzf2#fz%#fzf3#f
  endfunction
  autocmd! User FzfStatusLine call <SID>fzf_statusline()

  " Mapping ^p to fzf window
  map      <C-P>  <ESC>:Files ~\src\windows-codes<CR>

  " Mapping ^f to fzf window
  map      <C-F>  <ESC>:FZF<CR>

  " Mapping ^L to fzf window
  map      <C-L>  <ESC>:Lines<CR>

  " Mapping ^H to fzf window
  map      <C-H>  <ESC>:History:<CR>

  " Mapping ^B to fzf window
  map      <C-B>  <ESC>:Buffers<CR>

set completeopt=menuone,noinsert,noselect
let g:completion_matching_strategy_list = [ 'exact' , 'substring' , 'fuzzy' ]


lua << EOF
require('lspconfig').powershell_es.setup {
    bundle_path = 'C:/Users/A1936541/others/lsp/PowerShellEditorServices',
    on_attach=require'completion'.on_attach
}
EOF

"  require('lspconfig').powershell_es.setup{
"
"  }
"      bundle_path = 'C:/Users/A1936541/others/lsp/PowerShellEditorServices',


  "cmd={ 'powershell.exe', '-NoLogo', '-NoProfile', '-Command', "C:/Users/A1936541/others/lsp/PowerShellEditorServices/PowerShellEditorServices/Start-EditorServices.ps1 -BundledModulesPath  C:/Users/A1936541/others/lsp/PowerShellEditorServices -LogPath C:/Users/A1936541/lsp-logs/logs.log -SessionDetailsPath C:/Users/A1936541/lsp-logs/session.json -FeatureFlags @() -AdditionalModules @() -HostName 'ECENWDTI9440' -HostProfileId 'ecenwdti9440' -HostVersion 1.0.0 -Stdio -LogLevel Normal -EnableConsoleRepl" }


"lua require('lspconfig')..setup{}
" LSP
"nnoremap <silent> gdf   <cmd>lua vim.lsp.buf.definition()<CR>
"nnoremap <silent> gde   <cmd>lua vim.lsp.buf.declarations()<CR>
"nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
"nnoremap <silent> gi    <cmd>lua vim.lsp.buf.implementations()<CR>
"nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
"nnoremap <silent> <C-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
"nnoremap <silent> <C-p> <cmd>lua vim.lsp.buf.diagnostic.goto_prev()<CR>
"nnoremap <silent> <C-n> <cmd>lua vim.lsp.buf.diagnostic.goto_next()<CR>

autocmd FileType powershell inoremap ,if  If<Space>(<Space>$<Space>-<++><Space><++><Space>)<Space>{<CR><Space><Space><Space><Space><++><CR>}<Esc>d02k0f$a
autocmd FileType powershell inoremap ,ei  ElseIf<Space>(<Space>$<Space>-<++><Space><++><Space>)<Space>{<CR><Space><Space><Space><Space><++><CR>}<Esc>4h4x2k0f$a
autocmd FileType powershell inoremap ,el  Else<Space>{<CR><Space><Space><Space><Space><CR>}<Esc>4h4xkA
autocmd FileType powershell inoremap ,fe  ForEach<Space>(<Space>$<Space>in<Space>$<++><Space>)<Space>{<CR><Space><Space><Space><Space><++><CR>}<Esc>4h4x2k0f$a
autocmd FileType powershell inoremap ,fo  ''<Space>,<Space>'<++>'<Space>\|<Space>ForEach-Object<Space>-Process<Space>{<Space><++><Space>$_<Space>}<Esc>0f'i
autocmd FileType powershell inoremap ,wop Where-Object<Space>-Property<Space><Space>-<++><Space>$<++><Esc>0/Where-Object<CR>fyla
autocmd FileType powershell inoremap ,wof Where-Object<Space>-FilterScript<Space>{<CR><Space><Space><Space><Space>$_.<Space>-<++><Space><++><CR>}<Esc>0f.a
autocmd FileType powershell inoremap ,it  <Esc>BEa<Space>,<Space>''<Esc>i
autocmd FileType powershell inoremap ,and <Space>-and<CR>$_.
autocmd FileType powershell inoremap ,or  <Space>-or<CR>$_.
